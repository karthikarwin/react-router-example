import React from "react";
import { Route, BrowserRouter as Router } from "react-router-dom";
import Details from "./Details";
import "./App.css";
import MenuList from "./menu-list";
import Home from "./Home";

function App() {
  return (
    <Router>
      <div className="App">
        <div>
          <h1>Common Content</h1>
        </div>
        <Route exact path="/">
          <MenuList />
        </Route>

        <Route path="/details">
          <Details menu={"Details"} />
        </Route>

        <Route path="/home">
          <Home menu={"Home"} />
        </Route>
      </div>
    </Router>
  );
}

export default App;
