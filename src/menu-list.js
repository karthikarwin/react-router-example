import React from "react";
import { Link } from "react-router-dom";

function MenuList() {
  return (
    <div className="links">
      <Link to="/details">Details</Link>
      <br />
      <Link to="/home">Home</Link>
    </div>
  );
}

export default MenuList;
